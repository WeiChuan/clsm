#ifndef __LA_H__
#define __LA_H__

#include <vector>
#include <armadillo>

bool sparseMul(int winSize, const arma::SpMat<float> &features, const arma::Mat<float> &weight, arma::Mat<float> &output);
#endif
