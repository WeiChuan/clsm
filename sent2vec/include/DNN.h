#ifndef __DNN_H__
#define __DNN_H__

#include <string>
#include <map>
#include <vector>
#include <list>
#include "LFE.h"

enum ModelType
{
	MODEL_DSSM = 0,
	MODEL_CDSSM,
};

enum A_Func
{
	Linear = 0,
	Tanh = 1,
	Rectified = 2
};

enum N_Type
{
	Fully_Connected = 0,
	Convolution_layer = 1
};

enum P_Pooling
{
	MAX_Pooling = 0
};

class NeuralLayer
{
	public:
		int number;
		NeuralLayer(int num): number(num)
		{}
};

struct NeuralLink
{
		NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af);
		NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af, float isBias, N_Type nt, int winSize);
    	arma::Mat<float> weight;
    	arma::Mat<float> bias;
		NeuralLayer neuralIn;
		NeuralLayer neuralOut;
		N_Type nType;
		int nWinSize;
		P_Pooling poolType;
		A_Func Af;
		float isHidBias;
};

struct LayerOutput
{
		std::vector< arma::Row<float> > layerOutputs;
		std::vector< arma::Mat<float> > layerPooling;
};

class DNN
{
	public:
		DNN(ModelType type, const std::string &vocabfile, int maxTokenNum);
    	bool load(const std::string &modelName, const std::string &vocabFile);
		arma::Col<double> forward(std::list<std::string> &words);
		bool forwardActivate(const SampleInput &input, LayerOutput &output);
	private:
		ModelType _type;
		int _poolIdx;
		int _featureNum;
		int _maxPoolSentenceNumber;
		std::vector<NeuralLayer> _neuralLayers;
		LetterFeatureExtraction _LFE;
		std::vector<NeuralLink> _neuralLinks;
		bool loadModel(const std::string &fileName);
		bool loadVocab(const std::string &vocabFile);
};

#endif
