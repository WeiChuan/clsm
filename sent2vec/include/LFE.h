#ifndef __LEF_H__
#define __LEF_H__

#include <string>
#include <map>
#include <vector>
#include <list>
#include <armadillo>
#include "util.h"



class LetterFeatureExtraction
{
	public:
		LetterFeatureExtraction(int maxTokenNum);
		bool loadVocab(const std::string &fileName);
		std::map<int, float> toLetterNGramString(int ngram, std::vector<std::string> &words);
		bool getSOW(int ngram, const std::list<std::string> &words, arma::SpMat<float> &features);
	private:
		std::map<std::string, int> _vocabDict;
		int  _maxTokenNum;
};

struct SampleInput
{
	int batchSize;
	arma::SpMat<float> features;
	SampleInput(): batchSize(1)
	{};
};


#endif
