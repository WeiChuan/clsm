#include "LA.h"
#include <vector>

using namespace std;

bool sparseMul(int winSize, const arma::SpMat<float> &features, const arma::Mat<float> &weight, arma::Mat<float> &output)
{
  int ws = winSize / 2;
  int span = weight.n_rows / winSize;
  for (int i = 0; i < winSize; i++) {
    arma::Mat<float> W = weight.submat(i * span, 0, (i + 1) * span - 1, weight.n_cols - 1);
    arma::Mat<float> out = features * W;
    int head = i - ws;
    if (head < 0) {
      out.shed_rows(out.n_rows + head, out.n_rows - 1);
      out.insert_rows(0, -head, true);
    } else if (head > 0) {
      out.shed_rows(0, head - 1);
      out.insert_rows(out.n_rows, head, true);
    }
    if (output.n_cols == 0) {
      output = out;
    } else {
      output += out;
    }
  }
  return true;
}
