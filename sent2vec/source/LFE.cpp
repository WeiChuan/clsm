#include "LFE.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <list>

using namespace std;


/* Letter Feature Extraction  */
LetterFeatureExtraction::LetterFeatureExtraction(int maxTokenNum):
  _maxTokenNum(maxTokenNum)
{}

bool LetterFeatureExtraction::loadVocab(const string &fileName)
{
	FILE *vocabFp = NULL;
	char *line = NULL;
	size_t len = 0;
	int lineCnt = 0;
  bool ret = false;

	if (NULL == (vocabFp = fopen(fileName.c_str(), "r"))) {
		printf("failed to open[%s]\n", fileName.c_str());
		goto end;
	}

	while(-1 != getline(&line, &len, vocabFp)) {
		string fea = string(line);
		fea = rtrim(fea);
		_vocabDict[fea] = lineCnt;
		lineCnt++;
	}
	free(line);
  ret = true;
end:
	if (vocabFp) {
		fclose(vocabFp);
	}
  return ret;
}

map<int, float> LetterFeatureExtraction::toLetterNGramString(int ngram, vector<string> &words)
{
	map<int, float> feas;

	for(int i = 0; i <  words.size(); i++) {
		string mword = "#" + words[i] + "#";
		for (int j = 0; j < mword.size() - ngram + 1; j++) {
			string letterNGram = mword.substr(j, ngram);
			map<string, int>::iterator itL;
			if ((itL = _vocabDict.find(letterNGram)) != _vocabDict.end()) {
				int vindex = itL->second;
				map<int, float>::iterator itF;
				if ((itF = feas.find(vindex)) != feas.end()) {
					itF->second++;
				} else {
					feas[vindex] = 1;
				}
			}
		}
	}
	return feas;
}

static bool dict_to_feature(int feaNum, int feaDim, const list<map<int, float> > &dicts, arma::SpMat<float> &feature)
{
	arma::Mat<arma::uword> locs(2, feaNum);
	arma::Col<float> vals(feaNum);
	int rowIdx = 0;
	int feaCnt = 0;
	for(list<map<int, float> >::const_iterator dict = dicts.begin(); dict != dicts.end(); ++dict) {
		for (map<int, float>::const_iterator it = dict->begin(); it!= dict->end(); ++it) {
			locs(0, feaCnt) = rowIdx;
			locs(1, feaCnt) = it->first;
			vals(feaCnt) = it->second;
			feaCnt++;
		}
		rowIdx++;
	}
	feature = arma::SpMat<float>(locs, vals, dicts.size(), feaDim);
	return true;

}

bool LetterFeatureExtraction::getSOW(int ngram, const list<string> &words, arma::SpMat<float> &feature)
{
	list< map<int, float> > mdicts;

	int feaCnt = 0;
	int wordCnt = 0;
	for(list<string>::const_iterator it = words.begin(); it != words.end(); ++it) {
		map<int, float> tmp_p;
		string mword = "#" + *it + "#";
		for (int i = 0; i < mword.size() - ngram + 1; i++) {
			string nletter = mword.substr(i, ngram);
			map<string, int>::iterator itL;
			if ((itL = _vocabDict.find(nletter)) != _vocabDict.end()) {
				int vindex = itL->second;
				map<int, float>::iterator itF;
				if ((itF = tmp_p.find(vindex)) != tmp_p.end()){
					itF->second++;
				} else {
					tmp_p[vindex] = 1;
					feaCnt++;
				}
			}
		}
		mdicts.push_back(tmp_p);
		wordCnt++;
		if (wordCnt >= _maxTokenNum) {
			break;
		}
	}
	return dict_to_feature(feaCnt, _vocabDict.size(), mdicts, feature);
}

