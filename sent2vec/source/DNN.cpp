#include "DNN.h"
#include "util.h"
#include "LA.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <list>

using namespace std;

/*  Neural Link   */
NeuralLink::NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af):
	neuralIn(layerIn), neuralOut(layerOut), Af(af),
	nType(Fully_Connected), nWinSize(1), poolType(MAX_Pooling), isHidBias(0)
{}

NeuralLink::NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af, float isBias, N_Type nt, int winSize):
	neuralIn(layerIn), neuralOut(layerOut), Af(af),
	nType(nt), nWinSize(winSize), poolType(MAX_Pooling), isHidBias(isBias)
{}

/*  DNN */
DNN::DNN(ModelType type, const string &vocabfile, int maxTokenNum):
	_type(type), _maxPoolSentenceNumber(30), _poolIdx(0), _LFE(maxTokenNum)
{}

bool DNN::load(const std::string &modelName, const string &vocabFile)
{
  return loadModel(modelName) && loadVocab(vocabFile);
}

bool DNN::loadModel(const std::string &fileName)
{
#define READ_BYTES(size, buf, on_err) do {\
	if ((size) != fread(buf, 1, (size), fp)) { \
		printf("failed to read file[%s]\n", fileName.c_str()); \
		fclose (fp); \
		on_err; \
		return false; \
	}\
} while(0);

	FILE *fp = NULL;
	long lSize;
	char *buffer = NULL;
	size_t result = 0;

	fp = fopen(fileName.c_str(), "r");
	if (fp == NULL) {
		printf("failed to open[%s]\n", fileName.c_str());
		if (fp) {
			fclose(fp);
		}
		return false;
	}

	/* read mlayerNum (size of an int) */
	int mlayerNum = 0;
	READ_BYTES(sizeof(int), &mlayerNum, ;);

	/* read layerInfo (size of mlayerNum * int) */
	vector<int> layerInfo;
	buffer = (char*)malloc(sizeof(char) * mlayerNum * sizeof(int));
	if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}
	READ_BYTES(mlayerNum * sizeof(int), buffer, free(buffer));
	for (int i = 0; i < mlayerNum; i++) {
		layerInfo.push_back(*((int*)(buffer + sizeof(int) * i)));
	}
	_featureNum = layerInfo[0];
	free(buffer);

	for (int i = 0; i < mlayerNum; i++) {
		NeuralLayer layer(layerInfo[i]);
		_neuralLayers.push_back(layer);
	}

	/* read mlinkNum (size of an int) */
	int mlinkNum = 0;
	READ_BYTES(sizeof(int), &mlinkNum, ;);
	printf("%d\n", mlinkNum);

	/* read in neural links info and construct neural links  */
	for (int i = 0; i < mlinkNum; i++) {
		char params[7 * 4] = {};
		int offset = 0;
		READ_BYTES(sizeof(params), params, ;);
		int inNum = *((int*)(params + offset));//*((int*)(buffer + i*28));
		offset += 4;
		int outNum = *((int*)(params + offset));//*((int*)(buffer + i*28 + 4));
		offset += 4;
		float initHidBias = *((float*)(params + offset)); //(buffer + i*28 + 8));
		offset += 4;
		float initWeightSigma = *((float*)(params + offset));
		offset += 4;
		int mws = *((int*)(params + offset));  //m_Window_Size
		offset += 4;
		N_Type mnt = *((N_Type*)(params + offset));
		offset += 4;
		P_Pooling mp = *((P_Pooling*)(params + offset));
		offset += 4;
		printf("inNum = %d, outNum = %d, initHidBias = %e, mws = %d, mnt = %d, mp = %d\n", inNum, outNum, initHidBias, mws, mnt, mp);

		if (mnt == Convolution_layer) {
			NeuralLink link(_neuralLayers[i], _neuralLayers[i + 1], Tanh, 1, mnt, mws);
			_neuralLinks.push_back(link);
		} else if(mnt == Fully_Connected) {
			NeuralLink link(_neuralLayers[i], _neuralLayers[i + 1], Tanh);
			_neuralLinks.push_back(link);
		}
	}

	//  reading weight and bias
	for (int i = 0; i < mlinkNum; i++) {
		// reading weight
		int weightLen = 0;
		READ_BYTES(sizeof(int), &weightLen, ;);

		float *arr = (float*)malloc(sizeof(float) * weightLen);
		if (arr == NULL) {fputs ("Memory error",stderr); exit (2);}
		READ_BYTES(sizeof(float) * weightLen, arr, free(arr));
    	_neuralLinks[i].weight = arma::Mat<float>(arr, _neuralLinks[i].neuralOut.number, (int)weightLen / _neuralLinks[i].neuralOut.number);
		printf("matrix = %d * %d\n", _neuralLinks[i].neuralOut.number, (int)weightLen / _neuralLinks[i].neuralOut.number);
    	arma::inplace_trans(_neuralLinks[i].weight);

		free(arr);

		// reading bias
		int biasLen = 0;
		READ_BYTES(sizeof(int), &biasLen, ;);
		printf("bias Len = %d\n", biasLen);

		arr = (float*)malloc(sizeof(float) * biasLen);
		if (arr == NULL) {fputs ("Memory error",stderr); exit (2);}
		READ_BYTES(sizeof(float) * biasLen, arr, free(arr));
    	_neuralLinks[i].bias = arma::Row<float>(arr, biasLen);
		free(arr);
	}

	fclose (fp);
	return true;
}

bool DNN::loadVocab(const string &vocabFile)
{
	return _LFE.loadVocab(vocabFile);
}

arma::Col<double> DNN::forward(list<string> &words) {
	LayerOutput output;

	SampleInput input;

	//FIXME 還沒做 DSSM 的狀況(就是LFE 要用 BOW)
//  	arma::SpMat<float> features;
	_LFE.getSOW(3, words, input.features);
	forwardActivate(input, output);

	arma::Col<double> result(output.layerOutputs.back().n_cols);
	for(int r = 0; r < result.n_rows; r++) {
		result(r) = (double)output.layerOutputs.back()(r);
	}
	return result;
}

bool DNN::forwardActivate(const SampleInput &input, LayerOutput &output)
{
	 int layerIndex = 0;
	 int batchSize = input.batchSize;
	 for(int i = 0; i < _neuralLinks.size(); i++) {
		if(layerIndex == 0) {
			if(_neuralLinks[i].nType == Fully_Connected) {
//				printf("這次不會用到，之後再做\n");
			} else if (_neuralLinks[i].nType == Convolution_layer) {
				output.layerPooling.push_back(arma::Mat<float>());
				output.layerOutputs.push_back(arma::Row<float>());
        		sparseMul(_neuralLinks[i].nWinSize, input.features, _neuralLinks[i].weight, output.layerPooling.back());
        		output.layerOutputs.back() = arma::max(output.layerPooling.back(), 0);
			}
		} else {
			output.layerOutputs.push_back(arma::Row<float>());
			output.layerOutputs.back() = output.layerOutputs[layerIndex - 1] * _neuralLinks[i].weight;
		}
		if(_neuralLinks[i].Af == Tanh) {
			output.layerOutputs[layerIndex] = arma::tanh(output.layerOutputs[layerIndex]);	
			//output.layerOutputs[layerIndex].subvec(0, 5).print();
		}
		layerIndex++;
	 }
	return true;
}










