#ifndef __LEF_H__
#define __LEF_H__

#include <string>
#include <map>
#include <vector>
#include <list>
#include <armadillo>
#include "util.h"



class LetterFeatureExtraction
{
	public:
    LetterFeatureExtraction(int maxTokenNum);
		bool loadVocab(const std::string &fileName);
		std::map<int, float> toLetterNGramString(int ngram, std::vector<std::string> &words);
		std::list< std::map<int, float> > featureExtractorSOW(int ngram, const std::list<std::string> &words);
	private:
		std::map<std::string, int> _vocabDict;
		int  _maxTokenNum;
};

class SampleInput
{
	public:
		int batchSize;
		int segSize; // the total length of the full segments.
		int elementSize;

		std::vector<int> feaIdx;
		std::vector<float> feaValue;
		std::vector<int> sampleIdx; // length of the sequence.
		std::vector<int> segIdx; // the index of Segments.
		std::vector<int> segMargin;

		bool loadBOW(std::map<int, float> &fea);
		bool loadSOW(std::list< std::map<int, float> > &feas);
};

#endif
