#include "DNN.h"
#include "util.h"
#include "LA.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <list>

using namespace std;

/*  Neural Link   */
NeuralLink::NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af):
	neuralIn(layerIn), neuralOut(layerOut), Af(af),
	nType(Fully_Connected), nWinSize(1), poolType(MAX_Pooling), isHidBias(0)
{
	backWeight.reserve(layerIn.number * layerOut.number);
	backBias.reserve(layerOut.number);
}

NeuralLink::NeuralLink(NeuralLayer layerIn, NeuralLayer layerOut, A_Func af, float isBias, N_Type nt, int winSize):
	neuralIn(layerIn), neuralOut(layerOut), Af(af),
	nType(nt), nWinSize(winSize), poolType(MAX_Pooling), isHidBias(isBias)
{
	backWeight.reserve(layerIn.number * layerOut.number * winSize);
	backBias.reserve(layerOut.number);
}

/*  DNN */
DNN::DNN(ModelType type, const string &vocabfile, int maxTokenNum):
	_type(type), _maxPoolSentenceNumber(30), _poolIdx(0), _LFE(maxTokenNum)
{}

bool DNN::loadModel(const std::string &fileName)
{
#define READ_BYTES(size, buf, on_err) do {\
	if ((size) != fread(buf, 1, (size), fp)) { \
		printf("failed to read file[%s]\n", fileName.c_str()); \
		fclose (fp); \
		on_err; \
		return false; \
	}\
} while(0);

	FILE *fp = NULL;
	long lSize;
	char *buffer = NULL;
	size_t result = 0;

	fp = fopen(fileName.c_str(), "r");
	if (fp == NULL) {
		printf("failed to open[%s]\n", fileName.c_str());
		if (fp) {
			fclose(fp);
		}
		return false;
	}

	/* read mlayerNum (size of an int) */
	int mlayerNum = 0;
	READ_BYTES(sizeof(int), &mlayerNum, ;);

	/* read layerInfo (size of mlayerNum * int) */
	vector<int> layerInfo;
	buffer = (char*)malloc(sizeof(char) * mlayerNum * sizeof(int));
	if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}
	READ_BYTES(mlayerNum * sizeof(int), buffer, free(buffer));
	for (int i = 0; i < mlayerNum; i++) {
		layerInfo.push_back(*((int*)(buffer + sizeof(int) * i)));
	}
	_featureNum = layerInfo[0];
	free(buffer);

	for (int i = 0; i < mlayerNum; i++) {
		NeuralLayer layer(layerInfo[i]);
		_neuralLayers.push_back(layer);
	}

	/* read mlinkNum (size of an int) */
	int mlinkNum = 0;
	READ_BYTES(sizeof(int), &mlinkNum, ;);
	printf("%d\n", mlinkNum);

	/* read in neural links info and construct neural links  */
	for (int i = 0; i < mlinkNum; i++) {
		char params[7 * 4] = {};
		int offset = 0;
		READ_BYTES(sizeof(params), params, ;);
		int inNum = *((int*)(params + offset));//*((int*)(buffer + i*28));
		offset += 4;
		int outNum = *((int*)(params + offset));//*((int*)(buffer + i*28 + 4));
		offset += 4;
		float initHidBias = *((float*)(params + offset)); //(buffer + i*28 + 8));
		offset += 4;
		float initWeightSigma = *((float*)(params + offset));
		offset += 4;
		int mws = *((int*)(params + offset));  //m_Window_Size
		offset += 4;
		N_Type mnt = *((N_Type*)(params + offset));
		offset += 4;
		P_Pooling mp = *((P_Pooling*)(params + offset));
		offset += 4;
		printf("inNum = %d, outNum = %d, initHidBias = %e, mws = %d, mnt = %d, mp = %d\n", inNum, outNum, initHidBias, mws, mnt, mp);

		if (mnt == Convolution_layer) {
			NeuralLink link(_neuralLayers[i], _neuralLayers[i + 1], Tanh, 1, mnt, mws);
			neuralLinks.push_back(link);
		} else if(mnt == Fully_Connected) {
			NeuralLink link(_neuralLayers[i], _neuralLayers[i + 1], Tanh);
			neuralLinks.push_back(link);
		}
	}

	//  reading weight and bias
	for (int i = 0; i < mlinkNum; i++) {
		// reading weight
		int weightLen = 0;
		READ_BYTES(sizeof(int), &weightLen, ;);

		float *arr = (float*)malloc(sizeof(float) * weightLen);
		if (arr == NULL) {fputs ("Memory error",stderr); exit (2);}
		READ_BYTES(sizeof(float) * weightLen, arr, free(arr));
		neuralLinks[i].backWeight.assign(arr, arr + weightLen);

    printf("feature dim~~~ = %d\n", (int)weightLen / neuralLinks[i].neuralOut.number);
    neuralLinks[i].weight = arma::Mat<float>(arr, neuralLinks[i].neuralOut.number, (int)weightLen / neuralLinks[i].neuralOut.number);
    arma::inplace_trans(neuralLinks[i].weight);

		free(arr);

		// reading bias
		int biasLen = 0;
		READ_BYTES(sizeof(int), &biasLen, ;);

		arr = (float*)malloc(sizeof(float) * biasLen);
		if (arr == NULL) {fputs ("Memory error",stderr); exit (2);}
		READ_BYTES(sizeof(float) * biasLen, arr, free(arr));
		neuralLinks[i].backBias.assign(arr, arr + biasLen);
		free(arr);
	}

	fclose (fp);
	return true;
}

bool DNN::loadVocab(const string &vocabFile)
{
	if (vocabFile != "NULL" && vocabFile != "FEAT") {
		return _LFE.loadVocab(vocabFile);
	}
  return false;
}

vector<float> DNN::forward(list<string> &words) {
	LayerOutput output;

	SampleInput input;

	//FIXME 還沒做 DSSM 的狀況(就是LFE 要用 BOW)
	list< map<int, float> > feas = _LFE.featureExtractorSOW(3, words);
	input.loadSOW(feas);

	forwardActivate(input, output);

	vector<float> result(1,1);
//	for (int i = 0; i < output.layerOutputs[output.Layer_TOP].Length; i++)
//		result.Add(output.layerOutputs[output.Layer_TOP][i]);
	return result;
}

bool DNN::forwardActivate(SampleInput &input, LayerOutput &output)
{
	 int layerIndex = 0;
	 int batchSize = input.batchSize;
	 for(int i = 0; i < neuralLinks.size(); i++) {
		if(layerIndex == 0) {
//			printf("the first layer\n");
			if(neuralLinks[i].nType == Fully_Connected) {
//				printf("這次不會用到，之後再做\n");
			} else if (neuralLinks[i].nType == Convolution_layer) {
				output.layerPooling.push_back(vector<float>());
				output.layerOutputs.push_back(vector<float>());
				output.layerMaxPoolingIndex.push_back(vector<int>());
				ConvolutionSparseMatrixMultiply(input.sampleIdx, input.batchSize, input.segIdx,
						                        input.segMargin, input.segSize, input.feaIdx, input.feaValue, input.elementSize,
												neuralLinks[i].backWeight, output.layerPooling.back(),
												neuralLinks[i].neuralIn.number, neuralLinks[i].neuralOut.number, neuralLinks[i].nWinSize);
				MaxPooling(output.layerPooling.back(), input.sampleIdx, input.batchSize,
						   output.layerOutputs.back(), output.layerMaxPoolingIndex.back(), neuralLinks[i].neuralOut.number, 0);


        printf("===================\n");
        for (int m = 0; m < 7; m++) {
            printf("%f ", output.layerOutputs.back()[m]);
        }
			}
		} else {
//			printf("not the first layer\n");
		}

		if(neuralLinks[i].Af == Tanh) {
//			printf("tanh~\n");
		}
		layerIndex++;
	 }
	return true;
}










