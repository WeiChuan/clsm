#include "LA.h"
#include <vector>

using namespace std;

bool ConvolutionSparseMatrixMultiply(vector<int> &smpIndex, int batchSize, vector<int> &segIdx, vector<int> &segMargin, int segSize,
		                             vector<int> &feaIdx, vector<float> &feaValue, int elementSize,
		                             vector<float> &conWeight, vector<float> &output, int featureDim, int outputDim, int winSize)
{
	for (int i = 0; i < outputDim * segSize; i++) {
			int seg_idx = i / outputDim;  // 代表處理的是第幾個字的 vector
			int output_idx = i % outputDim; // 代表處理的是此 vector 的第幾維

			output.reserve(outputDim * segSize);
			int ws = winSize / 2;
			int mSmp_idx = segMargin[seg_idx];
			float sum = 0;
			for (int w = -ws; w <= ws; w++) {
				if ((seg_idx + w >= 0) && (seg_idx + w < segSize)) {
					if (segMargin[seg_idx + w] == mSmp_idx) {
						float mlen = 1;
						int row = seg_idx + w; //哪一個字
						int col_end = segIdx[row]; // col_begin and col_end 代表這個字的feature 是在 Fea 中的哪些位置
						int col_begin = 0;
						if (row > 0) {
							col_begin = segIdx[row - 1];
						}

						for (int j = col_begin; j < col_end; j++) {
							int fea_idx = feaIdx[j];
							if (fea_idx >= featureDim) // 超過 vocabulary 範圍
								continue;
							sum += feaValue[j] * 1.0f / mlen * conWeight[((w + ws) * featureDim + fea_idx) * outputDim + output_idx];
						}
					}
				}
			}
			output.push_back(sum);
	}
	return true;
}

bool MaxPooling(vector<float> &poolingFeas, vector<int> &smpIndex, int batchSize, vector<float> &output, vector<int> &maxpoolingIndex, int outputDim, int outputOff)
{
	for (int i = 0; i < outputDim * batchSize; i++) {
		if (i < outputDim * batchSize) {
			int batch_idx = i / outputDim;
			int output_idx = i % outputDim;
			int col_end = smpIndex[batch_idx];
			int col_begin = 0;
			if (batch_idx > 0) {
				col_begin = smpIndex[batch_idx - 1];
			}
			float max_value = 0;
			int max_index = -1;
			for (int j = col_begin; j < col_end; j++) {
				if ((max_index == -1) || (poolingFeas[j * outputDim + output_idx] > max_value)) {
					max_value = poolingFeas[j * outputDim + output_idx];
					max_index = j;
				}
			}
			output.push_back(max_value);
			maxpoolingIndex.push_back(max_index);
		}
	}
	return true;
}


