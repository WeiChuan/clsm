#include "LFE.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <list>

using namespace std;


/* Letter Feature Extraction  */
LetterFeatureExtraction::LetterFeatureExtraction(int maxTokenNum):
  _maxTokenNum(maxTokenNum)
{}

bool LetterFeatureExtraction::loadVocab(const string &fileName)
{
	FILE *vocabFp = NULL;
	char *line = NULL;
	size_t len = 0;
	int lineCnt = 0;
  bool ret = false;

	if (NULL == (vocabFp = fopen(fileName.c_str(), "r"))) {
		printf("failed to open[%s]\n", fileName.c_str());
		goto end;
	}

	while(-1 != getline(&line, &len, vocabFp)) {
		string fea = string(line);
		fea = rtrim(fea);
		_vocabDict[fea] = lineCnt;
		lineCnt++;
	}
	free(line);
  ret = true;
end:
	if (vocabFp) {
		fclose(vocabFp);
	}
  return ret;
}

map<int, float> LetterFeatureExtraction::toLetterNGramString(int ngram, vector<string> &words)
{
	map<int, float> feas;

	for(int i = 0; i <  words.size(); i++) {
		string mword = "#" + words[i] + "#";
		for (int j = 0; j < mword.size() - ngram + 1; j++) {
			string letterNGram = mword.substr(j, ngram);
			map<string, int>::iterator itL;
			if ((itL = _vocabDict.find(letterNGram)) != _vocabDict.end()) {
				int vindex = itL->second;
				map<int, float>::iterator itF;
				if ((itF = feas.find(vindex)) != feas.end()) {
					itF->second++;
				} else {
					feas[vindex] = 1;
				}
			}
		}
	}
	return feas;
}

list< map<int, float> > LetterFeatureExtraction::featureExtractorSOW(int ngram, const list<string> &words)
{
	list< map<int, float> > mdicts;

	map<int, float> tmp_p;
	for(list<string>::const_iterator it = words.begin(); it != words.end(); ++it) {
		if (mdicts.size() < _maxTokenNum){
			tmp_p.clear();
		}

		string mword = "#" + *it + "#";
		for (int i = 0; i < mword.size() - ngram + 1; i++) {
			string nletter = mword.substr(i, ngram);
			map<string, int>::iterator itL;
			if ((itL = _vocabDict.find(nletter)) != _vocabDict.end()) {
				int vindex = itL->second;
				map<int, float>::iterator itF;
				if ((itF = tmp_p.find(vindex)) != tmp_p.end()){
					itF->second++;
				} else {
					tmp_p[vindex] = 1;
				}
			}
		}

		if (tmp_p.size() > 0 && mdicts.size() < _maxTokenNum){
			mdicts.push_back(tmp_p);
		}
	}

//	for(list< map<int, float> >::iterator it1 = mdicts.begin(); it1 != mdicts.end(); ++it1) {
//		for(map<int, float>::iterator it2 = it1->begin(); it2 != it1->end(); ++it2) {
//			printf("%d, %f ||", it2->first, it2->second);
//		}
//		printf("\n");
//	}


	return mdicts;
}

/* SampleInput*/
bool SampleInput::loadBOW(map<int, float> &fea)
{
	sampleIdx.reserve(1);
	feaIdx.reserve(fea.size());
	feaValue.reserve(fea.size()) ;
	batchSize = 1; //read int four-byte.
	elementSize = fea.size();
	int i = 0;
	for(map<int, float>::iterator it = fea.begin(); it != fea.end(); ++it) {
		feaIdx[i] = it->first;
		feaValue[i] = it->second;
		i++;
	}
	sampleIdx[0] = elementSize;
	return true;
}

bool SampleInput::loadSOW(list< map<int, float> > &feas)
{
	batchSize = 1;
	segSize = feas.size();
	elementSize = 0;
	sampleIdx.reserve(1);

	segIdx.reserve(segSize);
	segMargin.reserve(segSize);
	int i = 0;
	for(list< map<int, float> >::iterator itL = feas.begin(); itL != feas.end(); ++itL) {
		elementSize += itL->size();
		segIdx.push_back(elementSize);
		segMargin.push_back(0);
		for (map<int, float>::iterator itM = itL->begin(); itM != itL->end(); ++itM) {
			feaIdx.push_back(itM->first);
			feaValue.push_back(itM->second);
		}
		i++;
	}
	sampleIdx.push_back(i);

	/*
	printf("segSize = %d\n", segSize);
	printf("elementSize = %d\n", elementSize);
	printf("sampleIdx[0] = %d\n", sampleIdx[0]);

	printf("\n");
	for(int j = 0; j < segIdx.size(); j++) {
		printf("segIdx[%d] = %d\n", j, segIdx[j]);
	}

	printf("\n");
	for(int j = 0; j < feaIdx.size(); j++) {
		printf("feaIdx[%d] = %d\n", j, feaIdx[j]);
		printf("feaValue[%d] = %f\n", j, feaValue[j]);

	}*/

	return true;
}















