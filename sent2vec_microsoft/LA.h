#ifndef __LA_H__
#define __LA_H__

#include <vector>
#include <armadillo>

bool ConvolutionSparseMatrixMultiply(std::vector<int> &smpIndex, int batchSize, std::vector<int> &segIdx, std::vector<int> &segMargin, int segSize,
		                             std::vector<int> &feaIdx, std::vector<float> &feaValue, int elementSize,
		                             std::vector<float> &conWeight, std::vector<float> &output, int FeatureDim, int outputDim, int winSize);
bool MaxPooling(std::vector<float> &poolingFeas, std::vector<int> &smpIndex, int batchSize,
		        std::vector<float> &output, std::vector<int> &maxpoolingIndex, int outputDim, int outputOff);

#endif
