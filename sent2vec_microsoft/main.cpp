#include <vector>
#include <string>
#include <fstream>
#include "DNN.h"
#include "util.h"

using namespace std;

#ifndef MAX_LINE
#	define MAX_LINE	256
#endif

static bool Embedding(const string &inSrcModel, const string &inSrcVocab, ModelType inSrcModelType, int inSrcMaxRetainedSeqLength,
		       const string &inTgtModel, const string &inTgtVocab, ModelType inTgtModelType, int inTgtMaxRetainedSeqLength,
		       const string &inFilename, const string &outFilenamePrefix)
{
	DNN src_dssm = DNN(inSrcModelType, inSrcVocab, inSrcMaxRetainedSeqLength);
	DNN tgt_dssm = DNN(inTgtModelType, inTgtVocab, inTgtMaxRetainedSeqLength);
	FILE *inFp = NULL;
	char *line = NULL;
	size_t len = 0;
	int linecnt = 0;

  bool ret = false;

  if (!src_dssm.loadModel(inSrcModel) || !src_dssm.loadVocab(inSrcVocab)) {
    printf("failed to load source model\n");
    goto end;
  }
  if (!tgt_dssm.loadModel(inTgtModel) || !tgt_dssm.loadVocab(inTgtVocab)) {
    printf("failed to load target model\n");
    goto end;
  }


	if (NULL == (inFp = fopen(inFilename.c_str(), "r"))) {
		printf("failed to open[%s]\n", inFilename.c_str());
		goto end;
	}

	while(-1 != getline(&line, &len, inFp)) {
		linecnt++;
		if(0 == linecnt % 1000000) printf("|");
		else if (0 == linecnt % 100000) printf(".");
		list<string> elems = split(line, '\t');
    if (elems.size() != 2) {
      printf("Each line in the input file must contain a pair of tab-separated strings\n");
    }
		elems.back().erase(remove(elems.back().begin(), elems.back().end(), '\n'), elems.back().end());  //FIXME remove newline in string (have better solution?)
		list<string> srcWords = split(elems.front(), ' ');
		list<string> tgtWords = split(elems.back(), ' ');

		vector<float> srcvec;
		srcvec = src_dssm.forward(srcWords);
	}
	free(line);

  ret = true;
end:
	if (inFp) {
		fclose(inFp);
	}
  return ret;
}


int main(int argc, char *argv[])
{
	if(argc < 8) {
		printf("Usage:\n./lala inSrcModel inSrcVocab inSrcModelType"
				"inTgtModel inTgtVocab inTgtModelType inFilename outFilenamePrefix\n");
		return 0;
	}

	const char *inSrcModel = argv[1];
	const char *inSrcVocab = argv[2];
	const char *inTgtModel = argv[4];
	const char *inTgtVocab = argv[5];
	const char *inFilename = argv[7];
	const char *outFilenamePrefix = argv[8];
	int inSrcMaxRetainedSeqLength = 30;
	int inTgtMaxRetainedSeqLength = 30;
	ModelType inSrcModelType;
	ModelType inTgtModelType;

	if (0 == strncmp(argv[3], "DSSM", 4)) {
		inSrcModelType = MODEL_DSSM;
	} else {
		inSrcModelType = MODEL_CDSSM;
	}

	if (0 == strncmp(argv[6], "DSSM", 4)) {
		inTgtModelType = MODEL_DSSM;
	} else {
		inTgtModelType = MODEL_CDSSM;
	}

	Embedding(inSrcModel, inSrcVocab, inSrcModelType, inSrcMaxRetainedSeqLength,
		      inTgtModel, inTgtVocab, inTgtModelType, inTgtMaxRetainedSeqLength,
		      inFilename, outFilenamePrefix);

	return 0;
}
