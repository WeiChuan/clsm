#ifndef __UTIL_H__
#define __UTIL_H__

#include <list>
#include <string>


std::list<std::string> &split(const std::string &s, char delim, std::list<std::string> &elems);
std::list<std::string> split(const std::string &s, char delim);

std::string &ltrim(std::string &s);
std::string &rtrim(std::string &s);
std::string &trim(std::string &s);



#endif
