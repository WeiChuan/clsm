#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

int main(int argc, char** argv)
{
  cout << "Armadillo version: " << arma_version::as_string() << endl;

  sp_mat A = sprandu<sp_mat>(2, 3, 0.1);  // directly specify the matrix size (elements are uninitialised)
  mat B = randu<mat>(3, 4);  // directly specify the matrix size (elements are uninitialised)
  (A * B).print();

}
